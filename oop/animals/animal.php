<?php

class Animal 
{
  public $legs = 2;
  public $cold_blooded = "false";
  public $name;
  public function __construct($param)
  {
    $this->name = $param;
  }
  public function getName()
  {
    return $this->name;
  }
  public function getLegs()
  {
    return $this->legs;
  }
  public function getColdBlooded()
  {
    return $this->cold_blooded;
  }  
}
?>